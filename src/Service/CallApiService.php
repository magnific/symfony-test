<?php
namespace App\Service;

use App\Entity\Employe;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    private $client;
    private $baseurl;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        $this->baseurl = 'http://127.0.0.1:8000/api/';
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     */
    public function getEmployes(): array
    {
        $response = $this->client->request(
            'GET',
            $this->baseurl.'employes'
        );

        return $response->toArray()['hydra:member'];
    }
    public function getEmployeById(Employe $employe): array
    {
        $response = $this->client->request(
            'GET',
            $this->baseurl.'employes/'.$employe->getId()
        );
//        dd($response->toArray());
        //dd($response->toArray()['experiences']);
        return $response->toArray();
    }

    public function getExperience(): array
    {
        $response = $this->client->request(
            'GET',
            $this->baseurl.'experiences'
        );

        return $response->toArray()['hydra:member'];
    }

    public function addEmployes(): array
    {
        $response = $this->client->request(
            'POST',
            $this->baseurl.'employes'
        );

        return $response->toArray()['hydra:member'];
    }

    public function addExperience(Employe $employe): array
    {
        $response = $this->client->request(
            'POST',
            $this->baseurl.'employes'
        );

        return $response->toArray()['hydra:member'];
    }
}